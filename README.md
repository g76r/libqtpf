ABOUT
=====

Libqtpf was a free software implementation of Parenthesis Format.
It's now part of libp6core, see https://gitlab.com/g76r/libp6core
